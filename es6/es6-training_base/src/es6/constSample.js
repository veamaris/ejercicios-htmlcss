function ejemploConst() {
    if (true) {
        const x = 'hola mundo';
    }

    console.log(x);
}

function ejemploConst2() {
    const x = 'primer const';

    if(true){
        const x = 'segundo const';
        console.log(x);
    }


    console.log(x);
}

export default function(){
    ejemploConst2();
    ejemploConst();
    
}