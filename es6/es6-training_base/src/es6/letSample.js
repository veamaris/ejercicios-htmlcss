

function ejemploVar() {
    if (true) {
        var x = 'hola Mundo';
    }

    console.log(x);
    
}

function ejemploLet() {
    if (true) {
        let x = 'hola mundo';
    }

    console.log(x);
}

export default function(){
    ejemploVar();
    ejemploLet();
}