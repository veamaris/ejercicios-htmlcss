'use strict';

//import letConstSamples from './01_letConstSamples';
//import stringSamples from './02_stringSamples';

import templateSample from './templateSample';
import letSample from './letSample';
import constSample from './constSample';

templateSample();
constSample();
letSample();


//letConstSamples();
//stringSamples();

export default {};
